import * as _ from 'lodash';
import { Planner } from '../instance';
import { Report } from 'models/report';
import { AxiosResponse } from 'axios';

function createReport (report: Report): Promise<Report> {
  return new Promise((resolve, reject) => {
    Planner.post('/Reports', report)
      .then((response: AxiosResponse<Report>) => resolve(response.data))
      .catch(error => reject(error));
  });
}

function getReports(): Promise<Report[]> {
  return new Promise((resolve, reject) => {
    Planner.get('/reports')
      .then((response: AxiosResponse<Report[]>) => resolve(response.data))
      .catch(error => reject(error.response.data));
  })
}

function patchReport(report: Report): Promise<void> {
  const reportId = _.get(report, 'id', null);
  if (!reportId) {
    return null;
  }
  return new Promise((resolve, reject) => {
    Planner.patch(`/reports/${reportId}`, report)
      .then(response => resolve(response.data))
      .catch(error => reject(error.response.data));
  });
}

function deleteReport(id: string): Promise<void> {
  return new Promise((resolve, reject) => {
    Planner.delete(`/reports/${id}`)
      .then(response => resolve(response.data))
      .catch(error => reject(error.response.data));
  });
}
const Reports = {
  createReport,
  getReports,
  patchReport,
  deleteReport,
};
export default Reports;