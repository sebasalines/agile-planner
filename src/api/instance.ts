import axios, { AxiosInstance } from 'axios';
import { Dictionary } from 'lodash';

class Api {
  axiosInstance: AxiosInstance;
  baseUrl: string;
  headers: Dictionary<string> = {
    'Content-Type': 'application/json',
  };
  constructor(url: string) {
    this.baseUrl = url;
    this.axiosInstance = axios.create({
      baseURL: url,
      timeout: 90000,
      headers: this.headers,
    });
  }

  setHeaders = (key: string, value: string) => {
    this.axiosInstance.defaults.headers[key] = value;
    return this;
  }
  
  get(path: string, opts?: any) {
    return this.axiosInstance.get(path, opts);
  }
  post(path: string, body: Object, opts?: any) {
    return this.axiosInstance.post(path, body, opts);
  }
  put(path: string, body: Object, opts?: any) {
    return this.axiosInstance.put(path, body, opts);
  }
  patch(path: string, body: Object, opts?: any) {
    return this.axiosInstance.patch(path, body, opts);
  }
  delete(path: string, opts?: any) {
    return this.axiosInstance.delete(path, opts);
  }
}

export const Planner = new Api('https://agile-planner-api.herokuapp.com');