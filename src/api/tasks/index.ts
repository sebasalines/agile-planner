import * as _ from 'lodash';
import { Planner } from '../instance';
import { ApiTask as Task } from 'models/task';
import { AxiosResponse } from 'axios';
import { isObject } from 'util';

function createTask (project: Task): Promise<Task> {
  return new Promise((resolve, reject) => {
    Planner.post('/tasks', project)
      .then((response: AxiosResponse<Task>) => resolve(response.data))
      .catch(error => reject(error));
  });
}

function getTasks(): Promise<Task[]> {
  return new Promise((resolve, reject) => {
    Planner.get('/tasks')
      .then((response: AxiosResponse<Task[]>) => resolve(response.data))
      .catch(error => reject(error.response.data));
  })
}

function patchTask(project: Task): Promise<void> {
  const projectId = _.get(project, 'id', null);
  if (!projectId) {
    return null;
  }
  return new Promise((resolve, reject) => {
    Planner.patch(`/tasks/${projectId}`, project)
      .then(response => resolve(response.data))
      .catch(error => reject(error.response.data));
  });
}

function deleteTask(id: string): Promise<void> {
  return new Promise((resolve, reject) => {
    Planner.delete(`/tasks/${id}`)
      .then(response => resolve(response.data))
      .catch(error => reject(error.response.data));
  });
}
const Tasks = {
  createTask,
  getTasks,
  patchTask,
  deleteTask,
};
export default Tasks;