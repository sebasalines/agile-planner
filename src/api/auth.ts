import { get } from 'lodash';
import { BaseUser, SignInPayload } from 'models/auth';
import { Planner } from './instance';
import { AxiosError } from 'axios';

export function signIn(data: { email: string, password: string }): Promise<SignInPayload | null> {
  return new Promise((res, rej) => {
    Planner.post('/users/login', data)
      .then(response => {
        const error = get(response, 'data.error', null);
        if (!error) {
          res(response.data);
        } else {
          rej(error)
        }
      })
      .catch((error: AxiosError) => rej(error.response.data));
  });
}