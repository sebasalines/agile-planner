import moment from 'moment';
import * as _ from 'lodash';
import { Planner } from '../instance';
import { TimeEntry as Model, TimeEntryFull as ModelFull, ApiTimeEntry as ApiModel } from 'models/time-entry';
import { AxiosResponse } from 'axios';
import { Task } from 'models/task';
import { AppStore } from 'models/store';
import { Project } from 'models/project';

class TimeEntry {
  tasks: Task[];
  projects: Project[];
  constructor(state: AppStore) {
    this.tasks = state.tasks.tasks;
    this.projects = state.projects.projects;
  }

  serialize(entry: Model): ApiModel {
    return {
      ...entry,
      task: entry.task,
      start: entry.start.toISOString(),
      duration: entry.duration.asMilliseconds(),
    };
  }

  unSerialize(entry: ApiModel): ModelFull {
    const task = this.tasks.find(t => t.id === entry.task);
    const project = this.projects.find(p => p.id === task.project);
    return {
      ...entry,
      task: {
        ...task,
        project,
        due: moment(task.due),
        estimate: moment.duration(task.estimate),
      },
      start: moment(entry.start),
      duration: moment.duration(entry.duration, 'milliseconds'),
    };
  }

  createTimeEntry(entry: Model): Promise<ApiModel> {
    return new Promise((resolve, reject) => {
      Planner.post('/time-entries', entry)
        .then((response: AxiosResponse<ApiModel>) => resolve(response.data))
        .catch(error => reject(error.response.data));
    });
  }

  getTimeEntries(): Promise<ApiModel[]> {
    return new Promise((resolve, reject) => {
      Planner.get('/time-entries')
        .then((response: AxiosResponse<ApiModel[]>) => {
          const entries: ApiModel[] = _.get(response, 'data', []);
          resolve(entries);
        })
        .catch(error => reject(error.response.data));
    })
  }

  patchTimeEntry(entry: Model): Promise<void> {
    const entryId = _.get(entry, 'id', null);
    if (!entryId) {
      return null;
    }
    return new Promise((resolve, reject) => {
      Planner.patch(`/time-entries/${entryId}`, this.serialize(entry))
        .then(response => resolve(response.data))
        .catch(error => reject(error.response.data));
    });
  }

  deleteTimeEntry(id: string): Promise<void> {
    return new Promise((resolve, reject) => {
      Planner.delete(`/time-entries/${id}`)
        .then(response => resolve(response.data))
        .catch(error => reject(error.response.data));
    });
  }
}
export default TimeEntry;