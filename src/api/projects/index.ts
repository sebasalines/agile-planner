import * as _ from 'lodash';
import { Planner } from '../instance';
import { Project } from 'models/project';
import { AxiosResponse } from 'axios';

function createProject (project: Project): Promise<Project> {
  return new Promise((resolve, reject) => {
    Planner.post('/projects', project)
      .then((response: AxiosResponse<Project>) => resolve(response.data))
      .catch(error => reject(error));
  });
}

function getProjects(): Promise<Project[]> {
  return new Promise((resolve, reject) => {
    Planner.get('/projects')
      .then((response: AxiosResponse<Project[]>) => resolve(response.data))
      .catch(error => reject(error.response.data));
  })
}

function patchProject(project: Project): Promise<void> {
  const projectId = _.get(project, 'id', null);
  if (!projectId) {
    return null;
  }
  return new Promise((resolve, reject) => {
    Planner.patch(`/projects/${projectId}`, project)
      .then(response => resolve(response.data))
      .catch(error => reject(error.response.data));
  });
}

function deleteProject(id: string): Promise<void> {
  return new Promise((resolve, reject) => {
    Planner.delete(`/projects/${id}`)
      .then(response => resolve(response.data))
      .catch(error => reject(error.response.data));
  });
}
const Projects = {
  createProject,
  getProjects,
  patchProject,
  deleteProject,
};
export default Projects;