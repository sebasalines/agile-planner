import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from '@atlaskit/button';
import Modal, { ModalTransition } from '@atlaskit/modal-dialog';
import ContentWrapper from '../components/ContentWrapper';
import PageTitle from '../components/PageTitle';
import { signOut, addProject, getProjects } from 'redux/actions';
import { compose } from 'redux';
import { withRouter } from 'react-router';
import { AppStore } from 'models/store';
import ProjectForm from 'components/Project/ProjectForm';
import { Project } from 'models/project';
import { Planner } from 'api';
import ProjectModal from 'modals/ProjectModal';

interface Props {
  projects: Project[];
  addProject: (project: Project) => any;
  getProjects: () => void;
  signOut: () => void;
}
class SettingsPage extends Component<Props> {
  state = {
    project: null,
    projects: [],
    modalOpen: false,
  };

  componentDidMount() {
    this.props.getProjects();
  }

  openModal = () => this.setState({ modalOpen: true })
  closeForm = () => this.setState({ modalOpen: false })

  onSaveProject = project => this.props.addProject(project)

  render() {
    return (
      <ContentWrapper>
        <PageTitle>Settings</PageTitle>
        <ProjectModal
          project={this.state.project}
          close={this.closeForm}
          isOpen={this.state.modalOpen}
        />
        {
          this.props.projects.map(project => {
            return (
              <span onClick={() => this.setState({ project, modalOpen: true })} style={{ padding: '10px', backgroundColor: project.color || '#F3F3F3' }}>
                {project.title}
              </span>
            )
          })
        }
        <Button onClick={this.openModal}>Add Project</Button>
        <Button onClick={this.props.signOut}>Sign Out</Button>
      </ContentWrapper>
    );
  }
}

const mapStateToProps = (state: AppStore) => ({
  projects: state.projects.projects,
});
const mapDispatchToProps = dispatch => ({
  signOut: () => dispatch(signOut()),
  getProjects: () => dispatch(getProjects()),
  addProject: project => dispatch(addProject(project)),
});
const enhance = compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)
export default enhance(SettingsPage);
