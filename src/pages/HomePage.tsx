import PropTypes from 'prop-types';
import * as Moment from 'moment';
import { get, includes } from 'lodash';
import styled from 'styled-components';
import React, { Component } from 'react';
import Button, { ButtonGroup } from '@atlaskit/button';
import MainSection from '../components/MainSection';
import ContentWrapper from '../components/ContentWrapper';
import PageTitle from '../components/PageTitle';
import TaskForm from '../components/Task/TaskForm';
import Modal, { ModalTransition } from '@atlaskit/modal-dialog';
import TaskCard from '../components/Task/TaskCard';
import Timeline from '../components/Timeline';
import { Task, PersistedTask } from '../models/task';
import DayPicker from 'components/DayPicker';
import { extendMoment } from 'moment-range';
import { Project } from 'models/project';
import TaskModal from 'modals/TaskModal';
import { AppStore } from 'models/store';
import { getTasks, deleteTask, updateTask, getTimeEntries, deleteTimeEntry, getFullTimeEntryData } from 'redux/actions';
import { connect } from 'react-redux';
import { TimeEntry, TimeEntryFull } from 'models/time-entry';
import TimeEntryModal from 'modals/TimeEntryModal';
import fullTimeEntries, { isLoadingFullTimeEntries } from 'redux/selectors/fullTimeEntries';
import ReportModal from 'modals/ReportModal';
import { Report } from 'models/report';

const moment = extendMoment(Moment);

type Props = {
  isLoading: boolean;
  tasks: Task[];
  timeEntries: TimeEntryFull[];
  getFullTimeEntryData: () => void;
  updateTask: (task) => void;
  deleteTask: (id) => void;
  deleteTimeEntry: (id) => void;
};
type State = {
  selectedDay: Moment.Moment;
  entry: TimeEntry | null;
  task: Task | null;
  report: Report | null;
  taskModalOpen: boolean;
  reportModalOpen: boolean;
  timeEntryModalOpen: boolean;
}

const TaskGroup = styled.div`
display: flex;
flex-direction: row;
flex-wrap: wrap;
`;

class HomePage extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      report: props.report || null,
      entry: props.entry || null,
      taskModalOpen: false,
      reportModalOpen: false,
      timeEntryModalOpen: false,
      selectedDay: moment(),
      task: props.task || null,
    };
  }
  componentDidMount() {
    this.props.getFullTimeEntryData();
  }

  setWeekDay = (selectedDay: Moment.Moment) => () => this.setState({ selectedDay })

  deleteTask = id => () => this.props.deleteTask(id)

  deleteTimeEntry = id => () => this.props.deleteTimeEntry(id)

  editTask = task => () => this.setState({ task, taskModalOpen: true })

  editTimeEntry = entry => () => this.setState({ entry, timeEntryModalOpen: true })

  openTaskForm = () => this.setState({ taskModalOpen: true, task: null })

  closeTaskForm = () => this.setState({ taskModalOpen: false })

  openTimeEntryForm = () => this.setState({ timeEntryModalOpen: true, entry: null })

  reportCloseForm = () => this.setState({ reportModalOpen: false })
  
  openReportForm = () => this.setState({ reportModalOpen: true })

  closeTimeEntryForm = () => this.setState({ timeEntryModalOpen: false })

  render() {
    return (
      <ContentWrapper>
        <PageTitle>Timeline</PageTitle>
        <ButtonGroup>
          <Button spacing="compact" onClick={this.openTaskForm}>Add Task</Button>
          <Button spacing="compact" onClick={this.openTimeEntryForm}>Add Time Entry</Button>
          <Button spacing="compact" onClick={this.openReportForm}>Get Daily Report</Button>
        </ButtonGroup>
        {
          !this.props.isLoading &&
          <section style={{ width: '100%' }}>
            <TaskModal
              task={this.state.task}
              close={this.closeTaskForm}
              isOpen={this.state.taskModalOpen}
            />
            <TimeEntryModal
              entry={this.state.entry}
              isOpen={this.state.timeEntryModalOpen}
              close={this.closeTimeEntryForm}
            />
            <ReportModal
              selectedDay={this.state.selectedDay}
              report={this.state.report}
              isOpen={this.state.reportModalOpen}
              close={this.reportCloseForm}
            />
            <TaskGroup>
              {
                this.props.tasks
                  .map(itm => {
                    return (
                      <TaskCard
                        task={itm}
                        onDelete={this.deleteTask(itm.id)}
                        onUpdate={this.editTask}
                        key={`taskCard_${itm.id}`}
                      />
                    );
                  })}
            </TaskGroup>
            <DayPicker
              selected={this.state.selectedDay}
              onClick={this.setWeekDay}
            />
            <Timeline
              isLoading={this.props.isLoading}
              date={this.state.selectedDay}
              entries={this.props.timeEntries}
            />
          </section>
        }
      </ContentWrapper>
    );
  }
}
const mapStateToProps = (state: AppStore) => ({
  tasks: state.tasks.tasks,
  isLoading: isLoadingFullTimeEntries(state),
  timeEntries: fullTimeEntries(state),
});
const mapDispatchToProps = dispatch => ({
  getFullTimeEntryData: () => dispatch(getFullTimeEntryData()),
  updateTask: task => dispatch(updateTask(task)),
  deleteTask: id => dispatch(deleteTask(id)),
  deleteTimeEntry: id => dispatch(deleteTimeEntry(id)),
});
export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
