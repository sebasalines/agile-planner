import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import App from './App';
import HomePage from '../pages/HomePage';
import SettingsPage from '../pages/SettingsPage';
import configStore from 'redux/store/configStore';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import PrivateRoute from 'components/PrivateRoute';
import LoginPage from 'pages/LoginPage';

import '../index.css';

const { store, persistor } = configStore();

export default class MainRouter extends Component<any, any> {
  constructor(props) {
    super(props);
    this.state = {
      navOpenState: {
        isOpen: true,
        width: 104,
      }
    }
  }

  getChildContext() {
    return {
      navOpenState: this.state.navOpenState,
    };
  }

  appWithPersistentNav = () => (props) => (
    <App
      onNavResize={this.onNavResize}
      {...props}
    />
  )

  onNavResize = (navOpenState) => {
    this.setState({
      navOpenState,
    });
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <HashRouter>
            <Switch>
              <Route component={LoginPage} path="/login" />
              <Route component={App} path="/" />
            </Switch>
          </HashRouter>
        </PersistGate>
      </Provider>
    );
  }
}