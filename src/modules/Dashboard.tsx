import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Flag, { FlagGroup } from '@atlaskit/flag';
import Modal from '@atlaskit/modal-dialog';
import Page from '@atlaskit/page';
import '@atlaskit/css-reset';

import StarterNavigation from '../components/StarterNavigation';
import { Switch, withRouter, Route } from 'react-router';
import PrivateRoute from 'components/PrivateRoute';
import HomePage from '../pages/HomePage';
import SettingsPage from '../pages/SettingsPage';
import { AppStore } from 'models/store';
import { connect } from 'react-redux';
import { compose } from 'redux';

class Dashboard extends Component<any> {
  state = {
    flags: [],
    isModalOpen: false,
  };

  static contextTypes = {
    navOpenState: PropTypes.object,
    router: PropTypes.object,
  };

  static propTypes = {
    navOpenState: PropTypes.object,
    onNavResize: PropTypes.func,
  };

  static childContextTypes = {
    showModal: PropTypes.func,
    addFlag: PropTypes.func,
  }

  componentDidMount() {
    if (!this.props.isLoggedIn) {
      this.props.history.push('/login');
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.isLoggedIn && this.props.isLoggedIn) {
      this.props.history.push('/login');
    }
  }
  
  getChildContext() {
    return {
      showModal: this.showModal,
      addFlag: this.addFlag,
    };
  }

  showModal = () => {
    this.setState({ isModalOpen: true });
  }

  hideModal = () => {
    this.setState({ isModalOpen: false });
  }

  addFlag = () => {
    this.setState({ flags: [{ id: Date.now() }].concat(this.state.flags) });
  }

  onFlagDismissed = (dismissedFlagId) => {
    this.setState({
      flags: this.state.flags.filter(flag => flag.id !== dismissedFlagId),
    })
  }

  render() {
    return (
      <div style={{ width: '100%' }}>
        <Page
          navigationWidth={104}
          navigation={<StarterNavigation />}
        >
          <Switch>
            <PrivateRoute path="/" exact component={HomePage} />
            <PrivateRoute path="/settings" component={SettingsPage} />
          </Switch>
        </Page>
        <div>
          <FlagGroup onDismissed={this.onFlagDismissed}>
            {
              this.state.flags.map(flag => (
                <Flag
                  id={flag.id}
                  key={flag.id}
                  title="Flag Title"
                  description="Flag description"
                />
              ))
            }
          </FlagGroup>
          {
            this.state.isModalOpen && (
              <Modal
                heading="Candy bar"
                actions={[{ text: 'Exit candy bar', onClick: this.hideModal }]}
                onClose={this.hideModal}
              >
                <p style={{ textAlign: 'center' }}>
                  <img src="http://i.giphy.com/yidUztgRB2w2gtDwL6.gif" alt="Moar cupcakes" />
                </p>
              </Modal>
            )
          }
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state: AppStore) => ({
  isLoggedIn: state.auth.isAuthenticated,
});
const enhance = compose(
  withRouter,
  connect(mapStateToProps),
)
export default enhance(Dashboard);
