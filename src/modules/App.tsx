import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Flag, { FlagGroup } from '@atlaskit/flag';
import Modal from '@atlaskit/modal-dialog';
import Page from '@atlaskit/page';
import '@atlaskit/css-reset';

import StarterNavigation from '../components/StarterNavigation';
import { Switch, withRouter, Route, Redirect } from 'react-router';
import PrivateRoute from 'components/PrivateRoute';
import HomePage from '../pages/HomePage';
import SettingsPage from '../pages/SettingsPage';
import { AppStore } from 'models/store';
import { connect } from 'react-redux';
import { compose } from 'redux';
import Dashboard from './Dashboard';
import LoginPage from 'pages/LoginPage';
import { Planner } from 'api';

class App extends Component<any> {
  render() {
    if (!this.props.isLoggedIn) {
      return <Redirect to="/login" />
    } else {
      Planner.setHeaders('Authorization', `Bearer ${this.props.token}`)
    }
    return (
      <Dashboard />
    );
  }
}
const mapStateToProps = (state: AppStore) => ({
  isLoggedIn: state.auth.isAuthenticated,
  token: state.auth.token,
});
const enhance = compose(
  withRouter,
  connect(mapStateToProps),
)
export default enhance(App);
