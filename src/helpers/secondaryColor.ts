import Color from 'color';
import { colors } from '@atlaskit/theme';

export default function (inputColor: string | null | undefined, ratio: number = .5) {
  const color = Color(inputColor || colors.N70);
  if (color.isDark()) {
    return color.lighten(ratio).hex();
  } else {
    return color.darken(ratio).hex();
  }
}