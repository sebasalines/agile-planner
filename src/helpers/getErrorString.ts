import { isString, get } from 'lodash';
import { ApiError } from 'models/api';

export interface ErrorResponse {
  error?: ApiError;
}
export default (error: ErrorResponse) => {
  if (isString(get(error, 'error.message'))) {
    alert(error.error.message);
  }
}