import { compact }  from 'lodash';
import moment from 'moment';
import { createSelector } from 'reselect';
import { AppStore } from '../../models/store';
import { TimeEntryFull } from 'models/time-entry';
import { TaskFull } from 'models/task';

const timeEntriesSelector = (state: AppStore) => state.timeEntries.entries;
const tasksSelector = (state: AppStore) => state.tasks.tasks;
const projectsSelector = (state: AppStore) => state.projects.projects;

export default createSelector(
  tasksSelector,
  projectsSelector,
  timeEntriesSelector,
  (tasks, projects, entries): TimeEntryFull[] => {
    const fullTasks: TaskFull[] = compact(tasks.map(t => {
      const project = projects.find(p => p.id === t.project);
      if (!project) {
        return undefined;
      }
      return {
        ...t,
        project,
        due: moment(t.due),
        estimate: moment.duration(t.estimate),
      };
    }));
    return compact(entries.map(entry => {
      const task = fullTasks.find(t => t.id === entry.task);
      if (!task) {
        return undefined;
      }
      return {
        ...entry,
        task,
        start: moment(entry.start),
        duration: moment.duration(entry.duration),
      };
    }));
  }
);

const isLoadingTimeEntriesSelector = (state: AppStore) => state.timeEntries.isLoading;
const isLoadingTasksSelector = (state: AppStore) => state.tasks.isLoading;
const isLoadingProjectsSelector = (state: AppStore) => state.projects.isLoading;

export const isLoadingFullTimeEntries = createSelector(
  isLoadingTimeEntriesSelector,
  isLoadingTasksSelector,
  isLoadingProjectsSelector,
  (entries, tasks, projects) => (
    entries || tasks || projects
  )
)