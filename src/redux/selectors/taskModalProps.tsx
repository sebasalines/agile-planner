import * as React from 'react';
import * as Moment from 'moment';
import { extendMoment } from 'moment-range';
import { createSelector } from 'reselect';
import { AppStore } from '../../models/store';
import { ItemsPropTypeSmart, ItemPropTypeSmart } from 'models/atlaskit';
import { ProjectLabel } from 'components/Project/UI';

const moment = extendMoment(Moment);

const projectsSelector = (state: AppStore) => state.projects.projects;

export interface TaskModalPropsSelectorData {
  availableIntervals: string[];
  projects: ItemsPropTypeSmart;
}
export default createSelector(
  projectsSelector,
  projects => {
    const rangeStart = moment().startOf('day').add(6, 'hours');
    const range = moment.range(rangeStart, moment(rangeStart).add(18, 'hours'));
    const availableIntervals = Array.from(range.by('minute', { step: 10 })).map(itm => itm.format('hh:mma'));
    return {
      availableIntervals,
      projects: projects.map(itm => {
        return {
          isDisabled: false,
          isSelected: false,
          label: <ProjectLabel project={itm}>{itm.title}</ProjectLabel>,
          name: 'project',
          value: itm.id,
        } as ItemPropTypeSmart;
      }),
    }
  }
)