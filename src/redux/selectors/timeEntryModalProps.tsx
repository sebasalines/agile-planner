import * as React from 'react';
import * as Moment from 'moment';
import { extendMoment } from 'moment-range';
import { createSelector } from 'reselect';
import { AppStore } from '../../models/store';
import { ItemsPropTypeSmart, ItemPropTypeSmart } from 'models/atlaskit';
import { ProjectLabel } from 'components/Project/UI';

const moment = extendMoment(Moment);

const tasksSelector = (state: AppStore) => state.tasks.tasks;
const projectsSelector = (state: AppStore) => state.projects.projects;

export interface TimeEntryModalPropsSelectorData {
  availableIntervals: string[];
  tasks: ItemsPropTypeSmart;
}
export default createSelector(
  tasksSelector,
  projectsSelector,
  (tasks, projects) => {
    const rangeStart = moment().startOf('day').add(6, 'hours');
    const range = moment.range(rangeStart, moment(rangeStart).add(18, 'hours'));
    const availableIntervals = Array.from(range.by('minute', { step: 10 })).map(itm => itm.format('hh:mma'));
    return {
      availableIntervals,
      tasks: tasks.map(itm => {
        const projectIndex = projects.findIndex(i => i.id === itm.project);
        return {
          isDisabled: false,
          isSelected: false,
          label: <ProjectLabel project={projects[projectIndex]}>{itm.title}</ProjectLabel>,
          name: 'project',
          value: itm.id,
        } as ItemPropTypeSmart;
      }),
    } as TimeEntryModalPropsSelectorData;
  }
)