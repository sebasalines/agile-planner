import { get, isString } from 'lodash';
import * as actions from '../constants';
import { signIn as login } from '../../api/auth';
import { ApiError } from 'models/api';
import { Planner } from 'api';

export const signIn = (email: string, password: string) => dispatch => {
	dispatch({ type: actions.GET_AUTH_TOKEN });
	return login({ email, password })
		.then(payload => {
			Planner.setHeaders('Authorization', `Bearer ${payload.token}`);
			dispatch({ type: actions.GET_AUTH_TOKEN_SUCCESS, data: { payload } });
		})
		.catch((error: { error: ApiError }) => {
			dispatch({ type: actions.GET_AUTH_TOKEN_FAILED, data: { error } });
			if (isString(get(error, 'error.message'))) {
				alert(error.error.message);
			}
		})
};

export const signOut = () => dispatch => {
	Planner.setHeaders('Authorization', '');
	dispatch({ type: actions.DESTROY_AUTH_TOKEN });
};