import moment from 'moment';
import * as actions from '../constants';
import { Task, ApiTask } from 'models/task';
import Tasks from 'api/tasks';
import getErrorString from 'helpers/getErrorString';

export const addTask = (data: ApiTask) => dispatch => {
  dispatch({ type: actions.CREATE_TASK });
  Tasks.createTask(data)
    .then(task => {
      dispatch({ type: actions.CREATE_TASK_SUCCESS, data: { task } })
    })
    .catch(error => {
      getErrorString(error);
      dispatch({ type: actions.CREATE_TASK_FAILED, data: { error } });
    })
}

export const updateTask = (task: ApiTask) => dispatch => {
  dispatch({ type: actions.UPDATE_TASK });
  Tasks.patchTask(task)
    .then(() => {
      dispatch({ type: actions.UPDATE_TASK_SUCCESS, data: { task } })
    })
    .catch(error => {
      getErrorString(error);
      dispatch({ type: actions.UPDATE_TASK_FAILED, data: { error } });
    })
}

export const deleteTask = (id: string) => dispatch => {
  dispatch({ type: actions.DELETE_TASK });
  Tasks.deleteTask(id)
    .then(() => {
      dispatch({ type: actions.DELETE_TASK_SUCCESS, data: { id } })
    })
    .catch(error => {
      getErrorString(error);
      dispatch({ type: actions.DELETE_TASK_FAILED, data: { error } });
    })
}

export const getTasks = () => dispatch => {
  dispatch({ type: actions.GET_TASKS });
  Tasks.getTasks()
    .then(tasks => dispatch({
      type: actions.GET_TASKS_SUCCESS, data: {
        tasks: tasks.map(itm => {
          return {
            ...itm,
            start: moment(itm.due),
            duration: moment.duration(itm.estimate, 'milliseconds'),
            type: 'projected',
          } as Task;
        })
      }
    }))
    .catch(error => {
      getErrorString(error);
      dispatch({ type: actions.GET_TASKS_FAILED, data: { error } });
    })
}