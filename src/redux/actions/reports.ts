import * as actions from '../constants';
import { Report } from 'models/report';
import getErrorString from 'helpers/getErrorString';
import Reports from '../../api/reports';


export const addReport = (data: Report) => dispatch => {
    dispatch({ type: actions.CREATE_REPORT });
    Reports.createReport(data)
      .then(report => {
        dispatch({ type: actions.CREATE_REPORT_SUCCESS, data: { report } })
      })
      .catch(error => {
        dispatch({ type: actions.CREATE_REPORT_FAILED, data: { error } });
      })
  }
  
  export const updateReport = (report: Report) => dispatch => {
    dispatch({ type: actions.UPDATE_REPORT });
    Reports.patchReport(report)
      .then(() => {
        dispatch({ type: actions.UPDATE_REPORT_SUCCESS, data: { report } })
      })
      .catch(error => {
        getErrorString(error);
        dispatch({ type: actions.UPDATE_REPORT_FAILED, data: { error } });
      })
  }
  
  export const deleteReport = (id: string) => dispatch => {
    dispatch({ type: actions.DELETE_REPORT });
    Reports.deleteReport(id)
      .then(() => {
        dispatch({ type: actions.DELETE_REPORT_SUCCESS, data: { id } })
      })
      .catch(error => {
        getErrorString(error);
        dispatch({ type: actions.DELETE_REPORT_FAILED, data: { error } });
      })
  }
  
  export const getReports = () => dispatch => {
    dispatch({ type: actions.GET_REPORT });
    Reports.getReports()
      .then(Reports => dispatch({ type: actions.GET_REPORT_SUCCESS, data: { Reports } }))
      .catch(error => {
        getErrorString(error);
      })
  }