import moment from 'moment';
import * as actions from '../constants';
import { TimeEntry, ApiTimeEntry } from 'models/time-entry';
import TimeEntryAPI from 'api/time-entry';
import getErrorString from 'helpers/getErrorString';
import { AppStore } from 'models/store';
import { getTasks } from './task';
import { getProjects } from './projects';

const withApi = (
  callback: (
    dispatch: (action: { type: string, data?: any, payload?: any }) => void,
    api: TimeEntryAPI,
    state: () => AppStore,
  ) => any,
) => (
  dispatch: (action: { type: string, data?: any, payload?: any }) => void,
  getState: () => AppStore
) => {
    const apiInstance = new TimeEntryAPI(getState());
    return callback(dispatch, apiInstance, getState);
  }

export const getFullTimeEntryData = () => dispatch => {
  dispatch(getTasks());
  dispatch(getProjects());
  dispatch(getTimeEntries());
}

export const addTimeEntry = (data: TimeEntry) => withApi((dispatch, api) => {
  dispatch({ type: actions.CREATE_TIME_ENTRY });
  api.createTimeEntry(data)
    .then(entry => {
      dispatch({ type: actions.CREATE_TIME_ENTRY_SUCCESS, data: { entry } })
    })
    .catch(error => {
      getErrorString(error);
      dispatch({ type: actions.CREATE_TIME_ENTRY_FAILED, data: { error } });
    })
})

export const updateTimeEntry = (entry: TimeEntry) => withApi((dispatch, api) => {
  dispatch({ type: actions.UPDATE_TIME_ENTRY });
  api.patchTimeEntry(entry)
    .then(() => {
      dispatch({ type: actions.UPDATE_TIME_ENTRY_SUCCESS, data: { entry } })
    })
    .catch(error => {
      getErrorString(error);
      dispatch({ type: actions.UPDATE_TIME_ENTRY_FAILED, data: { error } });
    })
})

export const deleteTimeEntry = (id: string) => withApi((dispatch, api) => {
  dispatch({ type: actions.DELETE_TIME_ENTRY });
  api.deleteTimeEntry(id)
    .then(() => {
      dispatch({ type: actions.DELETE_TIME_ENTRY_SUCCESS, data: { id } })
    })
    .catch(error => {
      getErrorString(error);
      dispatch({ type: actions.DELETE_TIME_ENTRY_FAILED, data: { error } });
    })
})

export const getTimeEntries = () => withApi((dispatch, api) => {
  dispatch({ type: actions.GET_TIME_ENTRIES });
  api.getTimeEntries()
    .then(entries => dispatch({
      type: actions.GET_TIME_ENTRIES_SUCCESS, data: { entries }
    }))
    .catch(error => {
      getErrorString(error);
      dispatch({ type: actions.GET_TIME_ENTRIES_FAILED, data: { error } });
    })
})