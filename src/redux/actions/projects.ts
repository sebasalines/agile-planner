import * as actions from '../constants';
import { Project } from "models/project";
import Projects from 'api/projects';
import getErrorString from 'helpers/getErrorString';

export const addProject = (data: Project) => dispatch => {
  dispatch({ type: actions.CREATE_PROJECT });
  Projects.createProject(data)
    .then(project => {
      dispatch({ type: actions.CREATE_PROJECT_SUCCESS, data: { project } })
    })
    .catch(error => {
      dispatch({ type: actions.CREATE_PROJECT_FAILED, data: { error } });
    })
}

export const updateProject = (project: Project) => dispatch => {
  dispatch({ type: actions.UPDATE_PROJECT });
  Projects.patchProject(project)
    .then(() => {
      dispatch({ type: actions.UPDATE_PROJECT_SUCCESS, data: { project } })
    })
    .catch(error => {
      getErrorString(error);
      dispatch({ type: actions.UPDATE_PROJECT_FAILED, data: { error } });
    })
}

export const deleteProject = (id: string) => dispatch => {
  dispatch({ type: actions.DELETE_PROJECT });
  Projects.deleteProject(id)
    .then(() => {
      dispatch({ type: actions.DELETE_PROJECT_SUCCESS, data: { id } })
    })
    .catch(error => {
      getErrorString(error);
      dispatch({ type: actions.DELETE_PROJECT_FAILED, data: { error } });
    })
}

export const getProjects = () => dispatch => {
  dispatch({ type: actions.GET_PROJECTS });
  Projects.getProjects()
    .then(projects => dispatch({ type: actions.GET_PROJECTS_SUCCESS, data: { projects } }))
    .catch(error => {
      getErrorString(error);
      dispatch({ type: actions.GET_TASKS_FAILED, data: { error } });
    })
}