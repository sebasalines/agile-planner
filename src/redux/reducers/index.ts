import { combineReducers } from 'redux';
import auth from './auth';
import tasks from './tasks';
import projects from './projects';
import reports from './reports';
import timeEntries from './time-entry';

const appState: any = combineReducers({
  auth,
  reports,
  tasks,
  projects,
  timeEntries,
});
export default appState;
