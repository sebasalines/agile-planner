import { get } from 'lodash';
import * as actions from '../constants';
import { Project } from 'models/project';

interface ProjectsReducer {
  isLoading: boolean;
  projects: Project[];
}
const initialState: ProjectsReducer = {
  isLoading: false,
  projects: [],
}

const projects = (state: ProjectsReducer = initialState, action: { type: string, data?: any }): ProjectsReducer => {
  const { type, data } = action;
  switch (type) {
    case actions.CREATE_PROJECT:
      return {
        ...state,
        isLoading: true,
      };
    case actions.CREATE_PROJECT_FAILED:
      return {
        ...state,
        isLoading: false,
      }
    case actions.CREATE_PROJECT_SUCCESS: {
      const project: Project | null = get(data, 'project', null);
      return {
        ...state,
        isLoading: false,
        projects: project ? [...state.projects, project] : state.projects,
      }
    }
    case actions.UPDATE_PROJECT:
      return {
        ...state,
        isLoading: true,
      };
    case actions.UPDATE_PROJECT_FAILED:
      return {
        ...state,
        isLoading: false,
      }
    case actions.UPDATE_PROJECT_SUCCESS: {
      const project: Project | null = get(data, 'project', null);
      return {
        ...state,
        isLoading: false,
        projects: project ? [...state.projects].map(itm => {
          if (project.id === itm.id) {
            return {
              ...itm,
              ...project,
            };
          }
          return itm;
        }) : state.projects,
      };
    }
    case actions.DELETE_PROJECT:
      return {
        ...state,
        isLoading: true,
      };
    case actions.DELETE_PROJECT_FAILED:
      return {
        ...state,
        isLoading: false,
      }
    case actions.DELETE_PROJECT_SUCCESS: {
      const id: string | null = get(data, 'id', null);
      return {
        ...state,
        isLoading: false,
        projects: id ? [...state.projects].filter(itm => itm.id !== id) : state.projects,
      };
    }
    case actions.GET_PROJECTS:
      return {
        ...state,
        isLoading: true,
      };
    case actions.GET_PROJECTS_FAILED:
      return {
        ...state,
        isLoading: false,
      };
    case actions.GET_PROJECTS_SUCCESS:{
      const projects: Project[] = get(data, 'projects', state.projects);
      return {
        ...state,
        isLoading: false,
        projects: [...projects],
      }
    }
    default:
      return state;
  }
}
export default projects;
