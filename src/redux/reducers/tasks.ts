import { get } from 'lodash';
import * as actions from '../constants';
import { Task } from 'models/task';

interface TaskReducer {
  isLoading: boolean;
  tasks: Task[];
}
const initialState: TaskReducer = {
  isLoading: false,
  tasks: [],
}

const tasks = (state: TaskReducer = initialState, action: { type: string, data?: any }): TaskReducer => {
  const { type, data } = action;
  switch (type) {
    case actions.CREATE_TASK:
      return {
        ...state,
        isLoading: true,
      };
    case actions.CREATE_TASK_FAILED:
      return {
        ...state,
        isLoading: false,
      }
    case actions.CREATE_TASK_SUCCESS: {
      const task: Task | null = get(data, 'task', null);
      return {
        ...state,
        isLoading: false,
        tasks: task ? [...state.tasks, task] : state.tasks,
      }
    }
    case actions.UPDATE_TASK:
      return {
        ...state,
        isLoading: true,
      };
    case actions.UPDATE_TASK_FAILED:
      return {
        ...state,
        isLoading: false,
      }
    case actions.UPDATE_TASK_SUCCESS: {
      const task: Task | null = get(data, 'task', null);
      return {
        ...state,
        isLoading: false,
        tasks: task ? [...state.tasks].map(itm => {
          if (task.id === itm.id) {
            return {
              ...itm,
              ...task,
            };
          }
          return itm;
        }) : state.tasks,
      };
    }
    case actions.DELETE_TASK:
      return {
        ...state,
        isLoading: true,
      };
    case actions.DELETE_TASK_FAILED:
      return {
        ...state,
        isLoading: false,
      }
    case actions.DELETE_TASK_SUCCESS: {
      const id: string | null = get(data, 'id', null);
      return {
        ...state,
        isLoading: false,
        tasks: id ? [...state.tasks].filter(itm => itm.id !== id) : state.tasks,
      };
    };
    case actions.GET_TASKS:
      return {
        ...state,
        isLoading: true,
      };
    case actions.GET_TASKS_FAILED:
      return {
        ...state,
        isLoading: false,
      };
    case actions.GET_TASKS_SUCCESS: {
      const tasks: Task[] = get(data, 'tasks', state.tasks);
      return {
        ...state,
        isLoading: false,
        tasks: [...tasks],
      }
    };
    default:
      return state;
  }
}
export default tasks;
