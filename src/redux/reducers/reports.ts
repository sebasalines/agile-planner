import * as actions from '../constants/reports';
import { get } from 'lodash';
import { Report } from 'models/report';

interface ReportsReducer {
  isLoading: boolean;
  reports: Report[];
}

const initialState: ReportsReducer = {
  isLoading: false,
  reports: []
}

const reports = (state: ReportsReducer = initialState, action: ({ type: string, data?: any })): ReportsReducer => {
  const { type, data } = action;
  switch (type) {
    case actions.CREATE_REPORT:
      return {
        ...state,
        isLoading: true
      }
    case actions.CREATE_REPORT_SUCCESS:
      const report: Report | null = get(data, 'report', null);
      return {
        ...state,
        isLoading: false,
        reports: report ? [...state.reports, report] : state.reports,
      }
    case actions.CREATE_REPORT_FAILED:
      return {
        ...state,
        isLoading: false
      }
    case actions.UPDATE_REPORT:
      return {
        ...state,
        isLoading: true,
      };
    case actions.UPDATE_REPORT_FAILED:
      return {
        ...state,
        isLoading: false,
      }
    case actions.UPDATE_REPORT_SUCCESS: {
      const report: Report | null = get(data, 'report', null);
      return {
        ...state,
        isLoading: false,
        reports: report ? [...state.reports].map(itm => {
          if (report.id === itm.id) {
            return {
              ...itm,
              ...report,
            };
          }
          return itm;
        }) : state.reports,
      };
    }
    case actions.DELETE_REPORT:
      return {
        ...state,
        isLoading: true,
      };
    case actions.DELETE_REPORT_FAILED:
      return {
        ...state,
        isLoading: false,
      }
    case actions.DELETE_REPORT_SUCCESS: {
      const id: string | null = get(data, 'id', null);
      return {
        ...state,
        isLoading: false,
        reports: id ? [...state.reports].filter(itm => itm.id !== id) : state.reports,
      };
    }
    case actions.GET_REPORT:
      return {
        ...state,
        isLoading: true,
      };
    case actions.GET_REPORT_FAILED:
      return {
        ...state,
        isLoading: false,
      };
    case actions.GET_REPORT_SUCCESS: {
      const reports: Report[] = get(data, 'reports', state.reports);
      return {
        ...state,
        isLoading: false,
        reports: [...reports],
      }
    }
    default:
      return state;
  }
}

export default reports;