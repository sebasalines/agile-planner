import { REHYDRATE } from 'redux-persist';
import { AuthReducer } from "../../models/store";
import * as actions from '../constants/auth';
import { Planner } from 'api';

const initialState: AuthReducer = {
  isLoading: false,
  isAuthenticated: false,
  token: null,
  userInfo: null,
};

const auth = (state: AuthReducer = initialState, action: { type: string, payload?: any, data?: _.Dictionary<any> }): AuthReducer => {
  const { type, data } = action;
  switch (type) {
    case REHYDRATE: {
      if (state.token) {
        Planner.setHeaders('Authorization', `Bearer ${action.payload.token}`);
      }
      return {
        ...state,
        isLoading: false,
      };
    }
    case actions.GET_AUTH_TOKEN:
      return {
        ...state,
        isLoading: true,
      };
    case actions.GET_AUTH_TOKEN_FAILED:
      return {
        ...state,
        isLoading: false,
      };
    case actions.GET_AUTH_TOKEN_SUCCESS: {
      const { token } = data.payload;
      return {
        ...state,
        token,
        isLoading: false,
        isAuthenticated: true,
      };
    }
    case actions.DESTROY_AUTH_TOKEN:
      return {
        ...state,
        isLoading: false,
        isAuthenticated: false,
        token: null,
        userInfo: null,
      }
    default:
      return state;
  }
}
export default auth;
