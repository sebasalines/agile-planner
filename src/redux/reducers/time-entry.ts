import { get } from 'lodash';
import * as actions from '../constants';
import { TimeEntryReducer } from 'models/store';
import { TimeEntry } from 'models/time-entry';

const initialState: TimeEntryReducer = {
  isLoading: false,
  entries: [],
}

const timeEntries = (state: TimeEntryReducer = initialState, action: { type: string, data?: any }): TimeEntryReducer => {
  const { type, data } = action;
  switch (type) {
    case actions.CREATE_TIME_ENTRY:
      return {
        ...state,
        isLoading: true,
      };
    case actions.CREATE_TIME_ENTRY_FAILED:
      return {
        ...state,
        isLoading: false,
      }
    case actions.CREATE_TIME_ENTRY_SUCCESS: {
      const entry: TimeEntry | null = get(data, 'entry', null);
      return {
        ...state,
        isLoading: false,
        entries: entry ? [...state.entries, entry] : state.entries,
      }
    }
    case actions.UPDATE_TIME_ENTRY:
      return {
        ...state,
        isLoading: true,
      };
    case actions.UPDATE_TIME_ENTRY_FAILED:
      return {
        ...state,
        isLoading: false,
      }
    case actions.UPDATE_TIME_ENTRY_SUCCESS: {
      const entry: TimeEntry | null = get(data, 'entry', null);
      return {
        ...state,
        isLoading: false,
        entries: entry ? [...state.entries].map(itm => {
          if (entry.id === itm.id) {
            return {
              ...itm,
              ...entry,
            };
          }
          return itm;
        }) : state.entries,
      };
    }
    case actions.DELETE_TIME_ENTRY:
      return {
        ...state,
        isLoading: true,
      };
    case actions.DELETE_TIME_ENTRY_FAILED:
      return {
        ...state,
        isLoading: false,
      }
    case actions.DELETE_TIME_ENTRY_SUCCESS: {
      const id: string | null = get(data, 'id', null);
      return {
        ...state,
        isLoading: false,
        entries: id ? [...state.entries].filter(itm => itm.id !== id) : state.entries,
      };
    };
    case actions.GET_TIME_ENTRIES:
      return {
        ...state,
        isLoading: true,
      };
    case actions.GET_TIME_ENTRIES_FAILED:
      return {
        ...state,
        isLoading: false,
      };
    case actions.GET_TIME_ENTRIES_SUCCESS: {
      const entries: TimeEntry[] = get(data, 'entries', state.entries);
      return {
        ...state,
        isLoading: false,
        entries: [...entries],
      }
    };
    default:
      return state;
  }
}
export default timeEntries;
