export const CREATE_REPORT = 'report/CREATE_REPORT';
export const CREATE_REPORT_SUCCESS = 'report/CREATE_REPORT_SUCCESS';
export const CREATE_REPORT_FAILED = 'report/CREATE_REPORT_FAILED';

export const UPDATE_REPORT = 'report/UPDATE_REPORT';
export const UPDATE_REPORT_SUCCESS = 'report/UPDATE_REPORT_SUCCESS';
export const UPDATE_REPORT_FAILED = 'report/UPDATE_REPORT_FAILED';

export const DELETE_REPORT = 'report/DELETE_REPORT';
export const DELETE_REPORT_SUCCESS = 'report/DELETE_REPORT_SUCCESS';
export const DELETE_REPORT_FAILED = 'report/DELETE_REPORT_FAILED';

export const GET_REPORT = 'report/GET_REPORT';
export const GET_REPORT_SUCCESS = 'report/GET_REPORT_SUCCESS';
export const GET_REPORT_FAILED = 'report/GET_REPORT_FAILED';

