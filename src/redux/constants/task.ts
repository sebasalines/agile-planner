export const CREATE_TASK = 'task/CREATE_TASK';
export const CREATE_TASK_FAILED = 'task/CREATE_TASK_FAILED';
export const CREATE_TASK_SUCCESS = 'task/CREATE_TASK_SUCCESS';

export const UPDATE_TASK = 'task/UPDATE_TASK';
export const UPDATE_TASK_FAILED = 'task/UPDATE_TASK_FAILED';
export const UPDATE_TASK_SUCCESS = 'task/UPDATE_TASK_SUCCESS';

export const DELETE_TASK = 'task/DELETE_TASK';
export const DELETE_TASK_FAILED = 'task/DELETE_TASK_FAILED';
export const DELETE_TASK_SUCCESS = 'task/DELETE_TASK_SUCCESS';

export const GET_TASKS = 'task/GET_TASKS';
export const GET_TASKS_FAILED = 'task/GET_TASKS_FAILED';
export const GET_TASKS_SUCCESS = 'task/GET_TASKS_SUCCESS';
