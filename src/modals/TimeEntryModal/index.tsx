import * as React from 'react';
import Spinner from '@atlaskit/spinner';
import Modal, { ModalTransition } from '@atlaskit/modal-dialog';
import { AppStore } from 'models/store';
import { addTimeEntry, updateTimeEntry, deleteTimeEntry } from 'redux/actions';
import { connect } from 'react-redux';
import timeEntryModalProps, { TimeEntryModalPropsSelectorData } from 'redux/selectors/timeEntryModalProps';
import TimeEntryForm from 'components/TimeEntry/TimeEntryForm';
import { TimeEntry } from 'models/time-entry';

export interface TimeEntryModalProps extends TimeEntryModalPropsSelectorData {
  isLoading: boolean;
  isOpen: boolean;
  entry: TimeEntry | null;
  close: () => void;
  addTimeEntry: (project: TimeEntry) => void;
  deleteTimeEntry: (id: string) => void;
  updateTimeEntry: (project: TimeEntry) => void;
}

const TimeEntryModal: React.FunctionComponent<TimeEntryModalProps> = React.memo(props => {
  const onSave = (project, isNew) => {
    if (isNew) {
      props.addTimeEntry(project);
    } else {
      props.updateTimeEntry(project);
    }
  }
  const onDelete = id => props.deleteTimeEntry(id);
  return props.isOpen ? (
    <ModalTransition>
      <Modal>
        {
          props.isLoading
            ? (
              <Spinner />
            ) : (
              <TimeEntryForm
                onSave={onSave}
                entry={props.entry}
                onDelete={onDelete}
                onCancel={props.close}
                tasks={props.tasks}
                availableIntervals={props.availableIntervals}
              />
            )
        }
      </Modal>
    </ModalTransition>
  ) : null;
});
const mapStateToProps = (state: AppStore) => ({
  ...timeEntryModalProps(state),
  isLoading: state.projects.isLoading,
})
const mapDispatchToProps = dispatch => ({
  addTimeEntry: project => dispatch(addTimeEntry(project)),
  deleteTimeEntry: id => dispatch(deleteTimeEntry(id)),
  updateTimeEntry: project => dispatch(updateTimeEntry(project)),
})
export default connect(mapStateToProps, mapDispatchToProps)(TimeEntryModal);
