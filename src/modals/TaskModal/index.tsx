import * as React from 'react';
import Spinner from '@atlaskit/spinner';
import Modal, { ModalTransition } from '@atlaskit/modal-dialog';
import TaskForm from '../../components/Task/TaskForm';
import { Task } from 'models/task';
import { AppStore } from 'models/store';
import { addTask, updateTask, deleteTask } from 'redux/actions';
import { connect } from 'react-redux';
import taskModalProps, { TaskModalPropsSelectorData } from 'redux/selectors/taskModalProps';

export interface TaskModalProps extends TaskModalPropsSelectorData {
  isLoading: boolean;
  isOpen: boolean;
  task: Task | null;
  close: () => void;
  addTask: (project: Task) => void;
  deleteTask: (id: string) => void;
  updateTask: (project: Task) => void;
}

const TaskModal: React.FunctionComponent<TaskModalProps> = React.memo(props => {
  const onSave = (project, isNew) => {
    if (isNew) {
      props.addTask(project);
    } else {
      props.updateTask(project);
    }
  }
  const onDelete = id => props.deleteTask(id);
  return props.isOpen ? (
    <ModalTransition>
      <Modal>
        {
          props.isLoading
            ? (
              <Spinner />
            ) : (
              <TaskForm
                onSave={onSave}
                task={props.task}
                onDelete={onDelete}
                onCancel={props.close}
                projects={props.projects}
                availableIntervals={props.availableIntervals}
              />
            )
        }
      </Modal>
    </ModalTransition>
  ) : null;
});
const mapStateToProps = (state: AppStore) => ({
  ...taskModalProps(state),
  isLoading: state.projects.isLoading,
})
const mapDispatchToProps = dispatch => ({
  addTask: project => dispatch(addTask(project)),
  deleteTask: id => dispatch(deleteTask(id)),
  updateTask: project => dispatch(updateTask(project)),
})
export default connect(mapStateToProps, mapDispatchToProps)(TaskModal);
