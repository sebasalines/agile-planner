import * as React from 'react';
import Spinner from '@atlaskit/spinner';
import Modal, { ModalTransition } from '@atlaskit/modal-dialog';
import ProjectForm from 'components/Project/ProjectForm';
import { Project } from 'models/project';
import { AppStore } from 'models/store';
import { addProject, updateProject, deleteProject } from 'redux/actions';
import { connect } from 'react-redux';

export interface ProjectModalProps {
  isLoading: boolean;
  isOpen: boolean;
  close: () => void;
  addProject: (project: Project) => void;
  deleteProject: (id: string) => void;
  updateProject: (project: Project) => void;
  project: Project | null;
}

const ProjectModal: React.FunctionComponent<ProjectModalProps> = React.memo(props => {
  const onSave = (project, isNew) => {
    if (isNew) {
      props.addProject(project);
    } else {
      props.updateProject(project);
    }
  }
  const onDelete = id => props.deleteProject(id);
  return props.isOpen ? (
    <ModalTransition>
      <Modal>
        {
          props.isLoading ? (
            <Spinner />
          ) : (
            <ProjectForm
              project={props.project}
              onSave={onSave}
              onDelete={onDelete}
              onCancel={props.close}
            />
          )
        }
      </Modal>
    </ModalTransition>
  ) : null;
});
const mapStateToProps = (state: AppStore) => ({
  isLoading: state.projects.isLoading,
})
const mapDispatchToProps = dispatch => ({
  addProject: project => dispatch(addProject(project)),
  deleteProject: id => dispatch(deleteProject(id)),
  updateProject: project => dispatch(updateProject(project)),
})
export default connect(mapStateToProps, mapDispatchToProps)(ProjectModal);
