import * as React from 'react';
import Spinner from '@atlaskit/spinner';
import Modal, { ModalTransition } from '@atlaskit/modal-dialog';
import { AppStore } from 'models/store';
import { addReport, updateReport, deleteReport } from 'redux/actions';
import { connect } from 'react-redux';
import { Report } from 'models/report';
import ReportForm from 'components/Report/ReportForm';
import * as Moment from 'moment';

export interface ReportModalProps {
  isLoading: boolean;
  isOpen: boolean;
  selectedDay: Moment.Moment;
  close: () => void;
  addReport: (report: Report) => void;
  deleteReport: (id: string) => void;
  updateReport: (report: Report) => void;
  report: Report;
}

const ReportModal: React.FunctionComponent<ReportModalProps> = React.memo(props => {
  const onSave = (report, isNew) => {
    if (isNew) {
      props.addReport(report);
    } else {
      props.updateReport(report);
    }
  }
  const onDelete = id => props.deleteReport(id);
  return props.isOpen ? (
    <ModalTransition>
      <Modal>
        {
          props.isLoading ? (
            <Spinner />
          ) : (
            <ReportForm
              selectedDay={props.selectedDay}
              report={props.report}
              onSave={onSave}
              onDelete={onDelete}
              onCancel={props.close}
            />
          )
        }
      </Modal>
    </ModalTransition>
  ) : null;
});
const mapStateToProps = (state: AppStore) => ({
  isLoading: state.projects.isLoading,
})
const mapDispatchToProps = dispatch => ({
  addReport: report => dispatch(addReport(report)),
  deleteReport: id => dispatch(deleteReport(id)),
  updateReport: report => dispatch(updateReport(report)),
})
export default connect(mapStateToProps, mapDispatchToProps)(ReportModal);
