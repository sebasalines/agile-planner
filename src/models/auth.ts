export interface BaseUser {
  objectId: string;
  username: string;
  email: string;
  password: string;
  createdAt: string;
  updatedAt: string;
}

export interface SignInPayload extends BaseUser {
  token: string;
}