import moment from 'moment';
import { Project } from './project';

export interface ApiTask {
  id: string;
  title: string;
  description?: string;
  due: string;
  estimate: number;
  project: string;
}
export interface Task extends ApiTask {

}

export interface TaskFull {
  id: string;
  title: string;
  description?: string;
  due: moment.Moment;
  estimate: moment.Duration;
  project: Project;
};

export type PersistedTask = {
  id: string;
  title: string,
  start: string;
  duration: string;
  project: string;
  type: 'projected' | 'actual';
};

