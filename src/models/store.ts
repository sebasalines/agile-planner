import { BaseUser } from "./auth";
import { Task } from "./task";
import { Project } from "./project";
import { TimeEntry } from "./time-entry";


export interface AuthReducer {
  isLoading: boolean;
  isAuthenticated: boolean;
  token: string | null;
  userInfo: BaseUser | null;
}

export interface TaskReducer {
  isLoading: boolean;
  tasks: Task[];
}

export interface ProjectsReducer {
  isLoading: boolean;
  projects: Project[];
}

export interface TimeEntryReducer {
  isLoading: boolean;
  entries: TimeEntry[];
}

export interface AppStore {
  auth: AuthReducer;
  tasks: TaskReducer;
  projects: ProjectsReducer;
  timeEntries: TimeEntryReducer;
}