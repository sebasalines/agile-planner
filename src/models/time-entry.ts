import moment from 'moment';
import { Task, TaskFull } from './task';

export interface ApiTimeEntry {
  id: string;
  task: string;
  start: string;
  duration: number;
  type: 'projected' | 'actual';
}

export interface TimeEntry {
  id: string;
  task: string;
  start: moment.Moment;
  duration: moment.Duration;
  type: 'projected' | 'actual';
};

export interface TimeEntryFull {
  id: string;
  task: TaskFull;
  start: moment.Moment;
  duration: moment.Duration;
  type: 'projected' | 'actual';
}