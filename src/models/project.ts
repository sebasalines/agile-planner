import moment from 'moment';

export type Project = {
  id?: string;
  title: string,
  color?: string;
  description?: string;
  // createdAt: string;
  // updatedAt: string;
};
