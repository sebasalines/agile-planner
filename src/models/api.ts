export interface ApiError {
  statusCode?: number;
  name?: string;
  message?: string;
}