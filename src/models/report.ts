import moment from 'moment';

export type Report = {
    id?: string;
    title: string,
    description?: string;
    createdAt: string;
    // updatedAt: string;
  };
  