import * as React from 'react';
import { Report } from 'models/report';
import * as Moment from 'moment';
import Button from '@atlaskit/button';

interface ReportFormProps {
    report: Report;
    selectedDay: Moment.Moment;
    onSave: (report: Report, isNew: boolean) => void;
    onDelete: (report) => void;
    onCancel: () => void;
}

export default class ReportForm extends React.Component<ReportFormProps> {
    constructor(props: ReportFormProps) {
        super(props);
    }
    
    render() {
        return(
            <React.Fragment>
                <h1>{this.props.selectedDay.get('date') }</h1>
                <Button onClick={this.props.onCancel} appearance="default">Cancel</Button>
            </React.Fragment>
        )
    }
}