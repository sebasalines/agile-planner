import * as React from 'react';
import * as _ from 'lodash';
import { get } from 'lodash';
import { parse, closestTo } from 'date-fns';
import Form, { FormHeader, Field, FormSection, FormFooter } from '@atlaskit/form';
import TextField from '@atlaskit/textfield';
import { DatePicker, DateTimePicker, TimePicker } from '@atlaskit/datetime-picker';
import FieldRadioGroup from '@atlaskit/field-radio-group';
import Button from '@atlaskit/button';
import styled from 'styled-components';
import uuid from 'uuid/v4';
import * as Moment from 'moment';
import { extendMoment } from 'moment-range';

import { Task, ApiTask } from '../../../models/task';
import { ItemsPropTypeSmart } from 'models/atlaskit';
import { DurationSlider } from 'components/TimeEntry/UI';
const moment = extendMoment(Moment);

const defaultTimeUnits: ItemsPropTypeSmart = [
  { name: 'timeUnit', label: 'Minutes', value: 'minutes' },
  { name: 'timeUnit', value: 'hours', label: 'Hours' },
];

const defaultTypes: ItemsPropTypeSmart = [
  { name: 'type', label: 'Projected', value: 'projected', defaultSelected: true },
  { name: 'type', value: 'actual', label: 'Actual' },
];

interface InnerFormTransaction {
  title: string;
  timeUnit: 'minutes' | 'hours';
  duration: number;
  project: string;
  type: 'projected' | 'actual';
}

interface FormTransaction extends InnerFormTransaction {
  start: Moment.Moment;
}

const FormContainer = styled.div`
  padding: 20px;
`;

type Props = {
  task: Task | null;
  projects: ItemsPropTypeSmart;
  availableIntervals: string[];
  onCancel(): void;
  onDelete(id: string): void;
  onSave(task: ApiTask, isNew: boolean): void;
};
const TaskForm: React.FunctionComponent<Props> = React.memo(props => {
  const [title, setTitle] = React.useState(props.task ? props.task.title : '');
  const [estimate, setEstimate] = React.useState(moment.duration(props.task ? props.task.estimate : 0, 'milliseconds'));
  const [project, setProject] = React.useState(props.task ? props.task.project : '');
  const [due, setDue] = React.useState<Moment.Moment>(props.task ? moment(props.task.due) : moment());
  const onSubmit = () => {
    const task: ApiTask = {
      id: props.task ? props.task.id : uuid() as string,
      due: due.toISOString(),
      title,
      project,
      estimate: estimate.asMilliseconds(),
    };
    if (project) {
      props.onSave(task, !props.task);
    } else {
      alert('select project');
    }
  }
  const onChangeDate = (iso: string) => {
    const date = moment(iso, 'YYYY-MM-DD\THH:mmZ');
    setDue(date);
  }
  const onChangeProject = e => {
    setProject(e.currentTarget.value);
  }
  const onDelete = () => props.onDelete(props.task.id);
  const projects = props.projects.map(i => ({ ...i, isSelected: i.value === project }));
  return (
    <FormContainer>
      <form onSubmit={onSubmit}>
        <FormHeader title="New Task" />
        <FormSection title="Info">
          <TextField
            placeholder="Title"
            value={title}
            onChange={e => setTitle(e.currentTarget.value)}
          />
          <DatePicker
            value={due.toISOString()}
            onChange={onChangeDate}
          />
          <DurationSlider value={estimate} onChange={setEstimate} />
          <FieldRadioGroup
            isRequired
            label="Project:"
            items={projects}
            value={project}
            onRadioChange={onChangeProject}
          />
        </FormSection>
        <FormFooter>
          {
            props.task &&
            <Button onClick={onDelete} appearance="danger">Delete</Button>
          }
          <Button onClick={props.onCancel} appearance="default">Cancel</Button>
          <Button onClick={onSubmit} appearance="primary">Save</Button>
        </FormFooter>
      </form>
    </FormContainer>
  );
});
export default TaskForm;
