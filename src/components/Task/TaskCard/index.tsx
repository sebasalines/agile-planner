import * as React from 'react';
import Color from 'color';
import moment from 'moment';
import Button, { ButtonGroup } from '@atlaskit/button';
import DeleteIcon from '@atlaskit/icon/glyph/cross';
import EditIcon from '@atlaskit/icon/glyph/edit';
import { colors, gridSize, fontFamily, fontSize } from '@atlaskit/theme';
import styled from 'styled-components';
import { Task } from '../../../models/task';
import { AppStore } from 'models/store';
import { Project } from 'models/project';
import { ProjectLabel } from 'components/Project/UI';
import { connect } from 'react-redux';
import secondaryColor from 'helpers/secondaryColor';

const Card = styled('div')<{ color: string, accentColor: string }>`
margin: ${gridSize() / 2}px ${gridSize()}px;
background: ${props => props.color};
flex: 1 200px 200px;
min-width: 200px;
border-radius: 4px;
display: flex;
flex-direction: column;
align-items: flex-start;
justify-content: center;
font-size: ${fontSize()};
font-family: ${fontFamily()};
padding: ${gridSize() * 1.5}px;
border: 1px solid ${props => secondaryColor(props.color, .3)};
border-bottom: none;
-webkit-box-shadow: 0px 5px 0px 0px ${props => props.accentColor};
-moz-box-shadow: 0px 5px 0px 0px ${props => props.accentColor};
box-shadow: 0px 5px 0px 0px ${props => props.accentColor};
color: ${props => secondaryColor(props.color, .8)};
h5 {
  color: ${props => secondaryColor(props.color, 1)};
  display: inline-flex;
  flex: 1;
  font-weight: bold;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
.label {
  margin: 0 5px;
  font-size: 14px;
  line-height: 24px;
  padding: 0 4px;
  background: ${colors.T200};
  border-radius: 4px;
}
.actions {
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
}
.date {
  font-size: 10px;
}
`;

type Props = {
  projects: Project[];
  task: Task;
  onDelete(id): void;
  onUpdate: (task: Task) => void;
}
const TaskCard: React.FunctionComponent<Props> = props => {
  const { task: { id, estimate, project: projectId, title, due } } = props;
  const projectIndex = props.projects.findIndex(proj => proj.id === projectId);
  const project = props.projects[projectIndex];
  const duration = moment.duration(estimate, 'millisecond');
  const durationString = duration.as('minutes') >= 60
    ? `${duration.as('hours')}hr`
    : `${duration.as('minutes')}min`;
  const projectColor = Color(project.color || colors.N70);
  const accentColor = projectColor.isDark()
    ? projectColor.lighten(.50)
    : projectColor.lighten(.10);
  return (
    <Card color={accentColor.hex()} accentColor={projectColor.hex()}>
      <div className="actions">
        <h5>{title}</h5>
        <ButtonGroup>
          <Button
            appearance="default"
            spacing="compact"
            onClick={props.onUpdate(props.task)}
            iconBefore={<EditIcon size="small" label="Edit task" />}
          />
          <Button
            appearance="default"
            spacing="compact"
            onClick={() => props.onDelete(id)}
            iconBefore={<DeleteIcon size="small" label="Delete task" />}
          />
        </ButtonGroup>
      </div>
      <ProjectLabel project={project}>{project.title}</ProjectLabel>
      <span className="date">Due: {`${moment(due).format('hh:mm A')} - ${durationString}`}</span>
    </Card>
  )
};
const mapStateToProps = (state: AppStore) => ({
  projects: state.projects.projects,
})
export default connect(mapStateToProps)(TaskCard);
