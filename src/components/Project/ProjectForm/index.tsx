import * as React from 'react';
import * as _ from 'lodash';
import { SwatchesPicker } from 'react-color';
import Form, { FormHeader, Field, FormSection, FormFooter } from '@atlaskit/form';
import TextField from '@atlaskit/textfield';
import TextArea from '@atlaskit/textarea';
import Button from '@atlaskit/button';
import styled from 'styled-components';
import * as Moment from 'moment';
import { extendMoment } from 'moment-range';

import { Project } from 'models/project';
const moment = extendMoment(Moment);

const CurrentColor = styled('div') <{ color: string }>`
width: 30px;
height: 22px;
margin: 0 10px;
border-radius: 4px;
background: ${props => props.color};
`;

type Props = {
  project: Project | null;
  onSave(project: Project, isNew: boolean): void;
  onCancel(): void;
  onDelete(id: string): void;
};
type State = {
  start: Moment.Moment;
};

interface FormTransaction {
  title: string;
  description: string;
  color: string;
}

const FormContainer = styled.div`
  padding: 20px;
`;

const ProjectForm: React.FunctionComponent<Props> = React.memo(props => {
  const [title, setTitle] = React.useState(props.project ? props.project.title : '');
  const [color, setColor] = React.useState(props.project ? props.project.color : '');
  const [description, setDescription] = React.useState(props.project ? props.project.description : '');
  const onSubmit = () => {
    const project: Project = {
      color,
      title,
      description,
    };
    props.onSave(props.project ? { ...props.project, ...project } : project, !props.project);
  }
  const onDelete = () => props.project ? props.onDelete(props.project.id) : null;
  const onChange = callback => e => callback(e.currentTarget.value);
  const onChangeColor = (color: { hex: string }) => setColor(color.hex);
  return (
    <FormContainer>
      <form onSubmit={onSubmit}>
        <FormHeader title="New Project" />
        <FormSection title="Info">
          <div>Color: <CurrentColor color={color} /></div>
          <TextField
            placeholder="Title"
            value={title}
            onChange={onChange(setTitle)}
          />
          <SwatchesPicker color={color} onChange={onChangeColor} />
          {
            // creo que hay algo mal en las props de <TextArea />
            // @ts-ignore
            <TextArea
              value={description}
              onChange={onChange(setDescription)}
            />
          }
        </FormSection>
        <FormFooter>
          {
            props.project &&
            <Button onClick={onDelete} appearance="danger">Delete</Button>
          }
          <Button onClick={props.onCancel} appearance="default">Cancel</Button>
          <Button onClick={onSubmit} appearance="primary">Save</Button>
        </FormFooter>
      </form>
    </FormContainer>
  );
});
export default ProjectForm;
