import Color from 'color';
import styled from 'styled-components';
import { colors } from '@atlaskit/theme';
import { Project } from 'models/project';
import secondaryColor from 'helpers/secondaryColor';

export const ProjectLabel = styled('span')<{ project: Project }>`
padding: 0 2px;
line-height: 20px;
font-size: 16px;
background: ${props => props.project.color || colors.N70 };
color: ${props => secondaryColor(props.project.color, .80)};
border: 1px solid ${props => secondaryColor(props.project.color, .5)};
text-transform: uppercase;
border-radius: 4px;
font-weight: bold;
`;