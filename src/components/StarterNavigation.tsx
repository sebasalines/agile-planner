import React, { Fragment } from 'react';
import { NavigationProvider, LayoutManager} from '@atlaskit/navigation-next';
import { Link } from 'react-router-dom';

import GlobalNavigation from '@atlaskit/global-navigation';
import AtlassianIcon from '@atlaskit/logo/AtlassianLogo/Icon';
import { Item, Separator, MenuSection, ItemAvatar } from '@atlaskit/navigation-next';


const MyGlobalNavigation = () => (
  <GlobalNavigation
    productIcon={AtlassianIcon}
    onProductClick={() => {}}
  />
);

const MyProductNavigation = () => (
  <Fragment>
    <Separator />
    <MenuSection>
      {({ className }) => (
        <div className={className}>
          <Link to="/settings">
            <Item text="Settings" />
          </Link>
          <Link to="/">
            <Item text="Timeline" />
          </Link>
        </div>
      )}
    </MenuSection>
  </Fragment>
);

export default class StarterNavigation extends React.Component {

  shouldComponentUpdate(nextProps, nextContext) {
    return true;
  };

  render() {

    return (
      <NavigationProvider>
        <LayoutManager
          globalNavigation={MyGlobalNavigation}
          productNavigation={MyProductNavigation}
          containerNavigation={null}
        >
        </LayoutManager>
      </NavigationProvider>
    );
  }
}
