import * as React from 'react';
import * as Moment from 'moment';
import { extendMoment } from 'moment-range';
import styled from 'styled-components';
import Button, { ButtonGroup } from '@atlaskit/button';

const moment = extendMoment(Moment);

const Container = styled.div`
width: 100%;
display: flex;
flex-direction: row;
align-items: center;
justify-content: space-between;
`;


interface DayPickerProps {
  selected: Moment.Moment;
  onClick: (day: Moment.Moment) => () => void;
}
const DayPicker: React.FunctionComponent<DayPickerProps> = React.memo(props => {
  const range = moment.range(moment().startOf('week'), moment().endOf('week'));
  const days = Array.from(range.by('day', { step: 1 }));
  return (
    <Container>
        {days.map(day => {
          return (
            <Button
              onClick={props.onClick(day)}
              appearance={moment(props.selected).startOf('day').isSame(moment(day).startOf('day')) ? 'primary' : 'default'}
              shouldFitContainer
            >
              {day.format('dddd')}
            </Button>
          );
        })}
    </Container>
  );
});
export default DayPicker;
