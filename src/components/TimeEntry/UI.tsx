import React from 'react';
import moment from 'moment';
import Range from '@atlaskit/range';

export interface DurationSliderProps {
  value: moment.Duration;
  onChange: (duration: moment.Duration) => void;
}
export const DurationSlider: React.FunctionComponent<DurationSliderProps> = React.memo(props => {
  const onChange = (minutes: number) => props.onChange(moment.duration(minutes, 'minutes'));
  const label = `${moment.utc(props.value.as('milliseconds')).format('HH:mm')} hours`;
  return (
    <>
      <label>{label}</label>
      <Range
        min={10}
        step={10}
        max={8 * 60}
        value={props.value.asMinutes()}
        onChange={onChange}
      />
    </>
  );
});
