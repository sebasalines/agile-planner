import * as React from 'react';
import * as _ from 'lodash';
import { get } from 'lodash';
import { parse } from 'date-fns';
import Form, { FormHeader, Field, FormSection, FormFooter } from '@atlaskit/form';
import TextField from '@atlaskit/textfield';
import { DatePicker, DateTimePicker, TimePicker } from '@atlaskit/datetime-picker';
import FieldRadioGroup from '@atlaskit/field-radio-group';
import Button from '@atlaskit/button';
import styled from 'styled-components';
import uuid from 'uuid/v4';
import * as Moment from 'moment';
import { extendMoment } from 'moment-range';

import { Task, ApiTask } from '../../../models/task';
import { ItemsPropTypeSmart } from 'models/atlaskit';
import { TimeEntry, ApiTimeEntry } from 'models/time-entry';
import { DurationSlider } from '../UI';
const moment = extendMoment(Moment);

const defaultTimeUnits: ItemsPropTypeSmart = [
  { name: 'timeUnit', label: 'Minutes', value: 'minutes' },
  { name: 'timeUnit', value: 'hours', label: 'Hours' },
];

const defaultTypes: ItemsPropTypeSmart = [
  { name: 'type', label: 'Projected', value: 'projected', defaultSelected: true },
  { name: 'type', value: 'actual', label: 'Actual' },
];

interface InnerFormTransaction {
  title: string;
  timeUnit: 'minutes' | 'hours';
  duration: number;
  project: string;
  type: 'projected' | 'actual';
}

const FormContainer = styled.div`
  padding: 20px;
`;

type Props = {
  entry: TimeEntry | null;
  tasks: ItemsPropTypeSmart;
  availableIntervals: string[];
  onCancel(): void;
  onDelete(id: string): void;
  onSave(entry: ApiTimeEntry, isNew: boolean): void;
};
const TimeEntryForm: React.FunctionComponent<Props> = React.memo(props => {
  const [task, setTask] = React.useState(props.entry ? props.entry.task : null);
  const [duration, setDuration] = React.useState(moment.duration(props.entry ? props.entry.duration : 0, 'milliseconds'));
  const [type, setType] = React.useState<'projected' | 'actual'>(props.entry ? props.entry.type : 'projected');
  const [start, setStart] = React.useState<Moment.Moment>(props.entry ? moment(props.entry.start) : moment());
  const onSubmit = () => {
    const entry: ApiTimeEntry = {
      type,
      task,
      id: props.entry ? props.entry.id : uuid() as string,
      start: start.toISOString(),
      duration: duration.asMilliseconds(),
    };
    props.onSave(entry, !props.entry);
  }
  const onChangeDate = (iso: string) => {
    const date = moment(iso, 'YYYY-MM-DD\THH:mmZ');
    setStart(date);
  }
  const onChangeTask = e => {
    setTask(e.currentTarget.value);
  }
  const onDelete = () => props.onDelete(props.entry.id);
  const types = defaultTypes.map(i => ({ ...i, isSelected: i.value === type }));
  const tasks = props.tasks.map(i => ({ ...i, isSelected: i.value === task }));
  return (
    <FormContainer>
      <form onSubmit={onSubmit}>
        <FormHeader title="New Task" />
        <FormSection title="Info">
          <FieldRadioGroup
            items={tasks}
            label="Task:"
            isRequired
            value={task}
            onRadioChange={onChangeTask}
          />
          <FieldRadioGroup
            items={types}
            label="Type:"
            isRequired
            value={type}
            onRadioChange={e => setType(e.currentTarget.value)}
          />
          <DateTimePicker
            timeIsEditable
            value={start.toISOString()}
            times={props.availableIntervals}
            onChange={onChangeDate}
          />
          <DurationSlider value={duration} onChange={setDuration} />
        </FormSection>
        <FormFooter>
          {
            props.entry &&
            <Button onClick={onDelete} appearance="danger">Delete</Button>
          }
          <Button onClick={props.onCancel} appearance="default">Cancel</Button>
          <Button onClick={onSubmit} appearance="primary">Save</Button>
        </FormFooter>
      </form>
    </FormContainer>
  );
});
export default TimeEntryForm;
