import React from 'react';
import { colors, gridSize, fontFamily, fontSize } from '@atlaskit/theme';
import styled from 'styled-components';
import * as Moment from 'moment';
import { extendMoment } from 'moment-range';
import { Task } from '../../models/task';
import { TimeEntry, TimeEntryFull } from 'models/time-entry';
import TimeEntryCard from './Card';

const moment = extendMoment(Moment);

const Container = styled.div`
display: flex;
flex-direction: column;
align-items: stretch;
height: auto;
width: 100%;
background: ${colors.N30};
padding: ${gridSize()};
overflow-x: scroll;
::-webkit-scrollbar-track {
    height: 5px;
    border-radius: 2px;
    background-color: ${colors.N20};
}
::-webkit-scrollbar {
    height: 5px;
    width: 5px;
    background-color: ${colors.N20};
}
::-webkit-scrollbar-thumb {
    height: 5px;
    width: 5px;
    border-radius: 5px;
    background-color: ${colors.N500};
}
`;

const BlockGroup = styled.div`
height: 100%;
flex: 1;
display: flex;
flex-direction: column;
justify-content: flex-start;
position: relative;
justify-content: space-between;
border-bottom: 4px solid ${colors.N200};
`;

type BlockProps = {

}
export const minuteWidth = 100 / 10;
export const minuteHeight = 22 / 10;

const Block = styled.div`
width: 100%;
display: flex;
text-align: center;
box-sizing: border-box;
border-bottom: 2px solid ${colors.N75};
z-index: 50;
:last-child {
  border-bottom-color: transparent;
}
.title {
  width: 100px;
  border-right: 2px solid ${colors.N75};
}
`;

const CardSection = styled('div')<{ index: number }>`
position:absolute;
top: 0;
left: 0;
width: 100%;
height: 100%;
z-index: ${props => 100 + props.index};
display: flex;
flex-direction: row;
`;

const CardGroup = styled.div`
flex: 1;
`;

type TimelineProps = {
  isLoading: boolean;
  date: Moment.Moment;
  entries: TimeEntryFull[];
};
const Timeline = (props: TimelineProps) => {
  if (props.isLoading) {
    return null;
  }
  const start = moment().startOf('day');
  const range = moment.range(start, moment(start).endOf('day'));
  const blocks = Array.from(range.by('minute', { step: 10 }));
  const cards = props.entries
    .filter(entry => moment(entry.start).startOf('day').isSame(moment(props.date).startOf('day')))
    .map((entry: TimeEntryFull) => {
      const diff = moment(props.date).startOf('day').diff(moment(entry.start), 'minutes');
      const offset = - diff * minuteHeight; // card-box-shadow/block border
      return {
        ...entry,
        offset,
      };
    })
    .reduce((cards, current) => {
      if (current.type === 'projected') {
        cards[0].push(current);
      } else if (current.type === 'actual') {
        cards[1].push(current);
      }
      return cards;
    }, [[], []]);
  return (
    <Container>
      <BlockGroup>
        <CardSection index={0}>
          {cards[0].map((card, index) => {
            const { offset, ...entry } = card;
            return (
              <TimeEntryCard entry={entry} offset={offset} zIndex={120 + index} />
            )
          })}
          {cards[1].map((card, index) => {
            const { offset, ...entry } = card;
            return (
              <TimeEntryCard entry={entry} offset={offset} zIndex={120 + index} />
            )
          })}
        </CardSection>
        {blocks.map(block => {
          return (
            <Block>
              <span className="title">{block.format('hh:mm A')}</span>
            </Block>
          );
        })}
      </BlockGroup>
    </Container>
  );
};
export default Timeline;