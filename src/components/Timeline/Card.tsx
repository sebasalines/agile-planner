import * as React from 'react';
import Color from 'color';
import moment from 'moment';
import Button, { ButtonGroup } from '@atlaskit/button';
import { gridSize, fontFamily, fontSize, colors } from '@atlaskit/theme';
import styled from 'styled-components';
import secondaryColor from 'helpers/secondaryColor';
import { ProjectLabel } from 'components/Project/UI';
import { connect } from 'react-redux';
import { AppStore } from 'models/store';
import { TimeEntry, TimeEntryFull } from 'models/time-entry';
import { Project } from 'models/project';
import { deleteTimeEntry } from 'redux/actions';
export const minuteHeight = 22 / 10;


const Card = styled('div')<{
  type: 'projected' | 'actual';
  offset: number;
  zIndex: number;
  duration: number;
  color: string;
  accentColor: string;
}>`
background: ${props => props.color};
flex: 1 200px 200px;
min-width: 200px;
border-radius: 4px;
display: flex;
flex-direction: column;
align-items: flex-start;
justify-content: center;
font-size: ${fontSize()};
font-family: ${fontFamily()};
padding: ${gridSize() * 1.5}px;
border: 1px solid ${props => secondaryColor(props.color, .3)};
border-bottom: none;
-webkit-box-shadow: -5px 0px 0px 0px ${props => props.accentColor};
-moz-box-shadow: -5px 0px 0px 0px ${props => props.accentColor};
box-shadow: -5px 0px 0px 0px ${props => props.accentColor};
color: ${props => secondaryColor(props.color, .8)};
height: ${props => `${props.duration * minuteHeight}px`};
top: ${props => props.offset}px;
left: ${props => props.type === 'projected' ? '107px' : 'calc(107px + 43%)'};
z-index: ${props => props.zIndex};
position: absolute;
overflow: hidden;
h5 {
  color: ${props => secondaryColor(props.color, 1)};
  display: inline-flex;
  flex: 1;
  font-weight: bold;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
.label {
  margin: 0 5px;
  font-size: 14px;
  line-height: 24px;
  padding: 0 4px;
  background: ${colors.T200};
  border-radius: 4px;
}
.actions {
  align-self: stretch;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
  z-index: ${props => props.zIndex + 10};
}
.date {
  font-size: 10px;
}
`;


interface Props {
  onDelete: (id: string) => void;
  entry: TimeEntryFull;
  zIndex: number;
  offset: number;
}
const TimeEntryCard: React.FunctionComponent<Props> = props => {
  if (!props.entry) {
    return null;
  }
  const { start, duration, task, type } = props.entry;
  const projectColor = Color(task.project.color || colors.N70);
  const accentColor = projectColor.isDark()
    ? projectColor.lighten(.50)
    : projectColor.lighten(.10);
  return (
    <Card
      type={type}
      offset={props.offset}
      zIndex={props.zIndex}
      color={accentColor.hex()}
      duration={duration.asMinutes()}
      accentColor={projectColor.hex()}
    >
      <div className="actions">
        <h5>{task.title}</h5>
        <ButtonGroup>
          <Button onClick={() => props.onDelete(props.entry.id)}>Delete</Button>
        </ButtonGroup>
      </div>
      <ProjectLabel project={task.project}>{task.project.title}</ProjectLabel>
      <span className="date">{`${start.format('hh:mm A')}`}</span>
      <span className="date">{duration.humanize()}</span>
    </Card>
  )
};
const mapStateToProps = (state: AppStore) => ({
  projects: state.projects.projects,
})
const mapDispatchToProps = dispatch => ({
  onDelete: id => dispatch(deleteTimeEntry(id)),
})
export default connect(null, mapDispatchToProps)(TimeEntryCard);
